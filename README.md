# Rhubarb Amazon SES Email Provider

##Quick Start
Below is an example configuration of this email provider
```php
// Generic AwsSettings
$aws = AwsSettings::singleton();
$aws->region = 'eu-west-1';
$aws->profile = 'default';

// Set credentials
// Using credentials.ini
$aws->iniCredentialsFile = '/path/to/credentials.ini';
// OR using access key and secret
$aws->credentialsAccessKeyId = 'abc123';
$aws->credentialsSecretAccessKey = 'abc123';
// OR Do not specify either of the above to use IAM role permission of the EC2 instance this code will run on

// Configure all the domains to send from with their matching ARN's from SES console
AmazonSesSettings::singleton()->arnMappings = [
    '<domain>' => 'arn:aws:ses:<region>:<account id>:identity/<domain>'
];

// Finally, set the provider
EmailProvider::setProviderClassName(AmazonSesEmailProvider::class);
```

## Bounce Handling
If you have deployed the [SES Handler](https://gitlab.com/gcdtech/ses-handler) tool to lambda, you can specify a lambda name
like so.
```php
AmazonSesSettings::singleton()->verificationLambdaName = '<verification lambda name>';
```

This will do out of the box bounce handling for email addresses and ensure emails that have produced a bounce in the past
do not get re-sent to. This tool grew out of an issue where an SES account was disabled due to a bounce rate which was too
 high. This tool also tracks bounce rate and will disable sending from a particular domain if the bounce rate exceeds a 
 safe limit, this is to ensure Amazon do not disable the SES account as this can take some time to re-enable. 
