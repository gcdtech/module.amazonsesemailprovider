<?php

namespace Gcdtech\AmazonSes\Exceptions;

class AmazonSesSettingsException extends AmazonSesException
{
    public function __construct()
    {
        parent::__construct("The current AmazonSesSettings are invalid.");
    }
}