<?php

namespace Gcdtech\AmazonSes\Exceptions;

class AmazonSesEmailNotValidException extends AmazonSesException
{
}
