<?php

namespace Gcdtech\AmazonSes\Exceptions;

class AmazonSesNoMatchingArnIdentityException extends AmazonSesException
{
    public function __construct($sender)
    {
        parent::__construct("The sender `$sender` does not have a corresponding ARN identity mapped in AmazonSesSettings");
    }
}