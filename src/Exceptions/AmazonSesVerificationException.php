<?php

namespace Gcdtech\AmazonSes\Exceptions;

class AmazonSesVerificationException extends AmazonSesException
{
    public function __construct()
    {
        parent::__construct('Invalid verification result');
    }
}
