<?php

namespace Gcdtech\AmazonSes\Exceptions;

class AmazonSesEmailBlockedException extends AmazonSesException
{
}
