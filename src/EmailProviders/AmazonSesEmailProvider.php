<?php

namespace Gcdtech\AmazonSes\EmailProviders;

use Gcdtech\AmazonSes\Exceptions\AmazonSesDomainDisabledException;
use Gcdtech\AmazonSes\Exceptions\AmazonSesEmailBlockedException;
use Gcdtech\AmazonSes\Exceptions\AmazonSesEmailNotValidException;
use Gcdtech\AmazonSes\Exceptions\AmazonSesNoMatchingArnIdentityException;
use Gcdtech\AmazonSes\Exceptions\AmazonSesRateLimitedException;
use Gcdtech\AmazonSes\Exceptions\AmazonSesVerificationException;
use Gcdtech\AmazonSes\Settings\AmazonSesSettings;
use Rhubarb\Crown\Sendables\Email\Email;
use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Sendables\Email\EmailRecipient;
use Rhubarb\Crown\Sendables\Sendable;

class AmazonSesEmailProvider extends EmailProvider
{
    const EVENT_DELIVERY = 'Delivery';
    const EVENT_BOUNCE = 'Bounce';
    const EVENT_COMPLAINT = 'Complaint';

    /**
     * Defaults to getting the arn from a mapping on the AmazonSesSettings class.
     *
     * You can get more flexibility by extending this class and overriding this method.
     *
     * @param EmailRecipient $sender
     * @return bool
     */
    protected function getArnMapping(EmailRecipient $sender)
    {
        $settings = AmazonSesSettings::singleton();
        $arn = false;

        $email = $sender->email;
        $parts = explode("@", $email, 2);

        $domain = $parts[1];

        if (isset($settings->arnMappings[$domain])) {
            $arn = $settings->arnMappings[$domain];
        }

        return $arn;
    }

    /**
     *
     * This will verify recipients using the verificationLambdaName if specified in the settings. Otherwise, this will
     * assume all recipients are valid
     *
     * @param EmailRecipient $recipients
     * @param $domainArn
     * @return bool
     * @throws AmazonSesEmailBlockedException
     * @throws AmazonSesDomainDisabledException
     * @throws AmazonSesRateLimitedException
     * @throws AmazonSesVerificationException
     * @throws \Rhubarb\Crown\Exceptions\RhubarbException
     * @throws \Rhubarb\Crown\Exceptions\SettingMissingException
     */
    public function verifyRecipient(EmailRecipient $recipient, $domainArn, $calls = 1): bool
    {
        $settings = AmazonSesSettings::singleton();
        if (!$settings->verificationLambdaName) {
            return true;
        }

        $payload = [
            'domainArn' => $domainArn,
            'email' => $recipient->email,
        ];

        $client = $settings->getLambdaClient();
        $response = json_decode($client->invoke([
            'FunctionName' => $settings->verificationLambdaName,
            'InvocationType' => 'RequestResponse',
            'LogType' => 'None',
            'Payload' => json_encode($payload),
        ])['Payload']->getContents());

        if ($response === true) {
            // email is good to send - happy days!
            return true;
        }

        if (isset($response->emailBlocked)) {
            throw new AmazonSesEmailBlockedException();
        }

        if (isset($response->domainSuspended)) {
            throw new AmazonSesDomainDisabledException();
        }

        if (isset($response->rateLimited)) {
            if ($calls >= $settings->verificationAttempts) {
                throw new AmazonSesRateLimitedException("Tried to send {$calls} times");
            }
            $calls++;
            // incremental back off
            usleep(((1 / $response->limit) * $calls) * 1000000);
            return $this->verifyRecipient($recipient, $domainArn, $calls);
        }

        // abort for invalid responses (if the user cares)
        if ($settings->alwaysRequireVerification) {
            throw new AmazonSesVerificationException();
        }
        // or we don't care about service outages/errors
        return true;
    }

    /**
     * Sends the sendable.
     *
     * Implemented by the concrete provider type.
     *
     * @param Sendable|Email $sendable
     * @return mixed
     * @throws AmazonSesEmailBlockedException
     * @throws AmazonSesDomainDisabledException
     * @throws AmazonSesEmailNotValidException
     * @throws AmazonSesNoMatchingArnIdentityException
     * @throws AmazonSesRateLimitedException
     * @throws AmazonSesVerificationException
     * @throws \Rhubarb\Crown\Exceptions\RhubarbException
     * @throws \Rhubarb\Crown\Exceptions\SettingMissingException
     */
    public function send(Sendable $sendable)
    {
        $settings = AmazonSesSettings::singleton();

        $arn = $this->getArnMapping($sendable->getSender());

        // Check we have a valid ARN
        if (!$arn) {
            throw new AmazonSesNoMatchingArnIdentityException($sendable->getSender()->getRfcFormat());
        }

        // Amazon require a text body.
        if (!$sendable->getText()) {
            throw new AmazonSesEmailNotValidException("Amazon require emails to have a text body.");
        }

        /** @var EmailRecipient[] $recipients */
        $recipients = $sendable->getRecipients();
        foreach ($recipients as $recipient) {
            try {
                $this->verifyRecipient($recipient, $arn);
            } catch (AmazonSesEmailBlockedException $exception) {
                // If this is a transaction, ignore that the address was blocked and send the email.
                if ($sendable->getPriority() !== Sendable::PRIORITY_TRANSACTION) {
                    throw $exception;
                }
            }
        }

        $message = $sendable->getMimeDocument();
        $rawMime = $message->getDocumentAsString();

        $client = $settings->getAwsClient();

        $request = [
            // Source is required
            'RawMessage' => [
                'Data' => $rawMime,
            ],
            // Source is required
            'Source' => $sendable->getSender()->getRfcFormat(),
            // Destinations is required
            'Destinations' => $recipients,
            'ReplyToAddresses' => [(string)$sendable->getReplyToRecipient()],
            'ReturnPath' => (string)$sendable->getSender(),
            'SourceArn' => $arn,
            'ReturnPathArn' => $arn,
        ];

        $response = $client->sendRawEmail($request);
        return $response->get("MessageId");
    }
}
