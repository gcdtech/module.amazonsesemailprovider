<?php

namespace Gcdtech\AmazonSes\UrlHandlers;

use Gcdtech\AmazonSes\EmailProviders\AmazonSesEmailProvider;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Request\WebRequest;
use Rhubarb\Crown\Response\HtmlResponse;
use Rhubarb\Crown\Response\Response;
use Rhubarb\Crown\UrlHandlers\UrlHandler;

class NotificationUrlHandler extends UrlHandler
{
    /**
     * Return the response if appropriate or false if no response could be generated.
     *
     * @param mixed $request
     * @return bool|Response
     */
    protected function generateResponseForRequest($request = null)
    {
        /**
         * @var WebRequest $request
         */
        $json = file_get_contents("php://input");
        $data = json_decode($json);

        if ($data->Type == "SubscriptionConfirmation"){
            return $this->handleSubscriptionRequest($data);
        } elseif ($data->Type == "Notification"){
            return $this->handleNotification($data);
        }

        $response = new HtmlResponse($this);
        $response->setContent("OK");

        return $response;
    }

    private function handleSubscriptionRequest($data)
    {
        $url = $data->SubscribeURL;
        $content = file_get_contents($url);

        $response = new HtmlResponse();

        if (!$content){
            $response->setResponseCode(501);
            $response->setResponseMessage("Error - could not retrieve subscription URL");
            return $response;
        }

        $response->setContent("OK");
        return $response;
    }

    private function handleNotification($data)
    {
        $message = json_decode($data->Message);
        $notificationType = $message->notificationType;

        $messageId = $message->mail->messageId;

        switch($notificationType){
            case AmazonSesEmailProvider::EVENT_DELIVERY:
                $this->recordDeliveryEvent($messageId, new RhubarbDateTime($message->delivery->timestamp), $message->delivery->recipients[0]);
                break;
            case AmazonSesEmailProvider::EVENT_BOUNCE:
                $this->recordBounceEvent($messageId, new RhubarbDateTime($message->bounce->timestamp), $message->bounce->bounceType == "Permanent", $message->bounce->bounceSubType, $message->bounce->bouncedRecipients[0]->emailAddress);
                break;
            case AmazonSesEmailProvider::EVENT_COMPLAINT:
                $this->recordComplaintEvent($messageId, new RhubarbDateTime($message->complaint->timestamp), $message->complaint->complaintFeedbackType, $message->complaint->complainedRecipients[0]->emailAddress);
                break;
        }

        $response = new HtmlResponse();
        $response->setContent("OK");

        return $response;
    }

    protected function recordDeliveryEvent(string $messageId, RhubarbDateTime $date, string $emailAddress)
    {

    }

    protected function recordBounceEvent(string $messageId, RhubarbDateTime $date, bool $permanent = false, string $bounceSubType = "", string $emailAddress)
    {

    }

    protected function recordComplaintEvent(string $messageId, RhubarbDateTime $date, string $feedback = "", string $emailAddress)
    {

    }
}