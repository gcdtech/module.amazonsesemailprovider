<?php

namespace Gcdtech\AmazonSes\Settings;

use Aws\Common\Aws;
use Aws\Lambda\LambdaClient;
use Aws\Ses\SesClient;
use Gcdtech\AmazonSes\Exceptions\AmazonSesSettingsException;
use Gcdtech\Aws\Settings\AwsSettings;
use Rhubarb\Crown\Settings;

class AmazonSesSettings extends Settings
{
    /**
     * Register ARN identities by mapping them using the sender domain as a key and the ARN as a value.
     *
     * e.g.
     *
     * $arnMappings['smith.com'] = 'arn:aws:ses:eu-west-1:142543242523:john/smith.com';
     *
     * @var array
     */
    public $arnMappings = [];

    /**
     * Name of the lambda deployed by https://gitlab.com/gcdtech/ses-proxy tool
     *
     * This will provide bounce detection for emails and rate limiting for a SES domain as a whole
     *
     * @var string|bool $verificationLambdaName
     */
    public $verificationLambdaName = false;
    /**
     * @var int The number of times for a single verification that the domain can be rate limited before throwing an exception.
     */
    public $verificationAttempts = 5;

    /**
     * @var bool TRUE will throw an exception if the verification lambda returns an unknown response.
     * Otherwise unknown responses (caused by service outages, errors, etc) will result in the email being sent without
     * verification and service checks.
     */
    public $alwaysRequireVerification = false;

    /**
     * @return SesClient
     * @throws \Rhubarb\Crown\Exceptions\RhubarbException
     * @throws \Rhubarb\Crown\Exceptions\SettingMissingException
     */
    public function getAwsClient()
    {
        $awsSettings = AwsSettings::singleton();
        $settings = $awsSettings->getClientSettings();

        return new SesClient($settings);
    }

    /**
     * @return LambdaClient
     * @throws \Rhubarb\Crown\Exceptions\RhubarbException
     * @throws \Rhubarb\Crown\Exceptions\SettingMissingException
     */
    public function getLambdaClient()
    {
        $awsSettings = AwsSettings::singleton();
        $settings = $awsSettings->getClientSettings();

        return new LambdaClient($settings);
    }
}
