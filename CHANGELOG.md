# Changelog

### 1.1.1

Changed:    No longer skips send attempt for blocked transactional emails (per Amazon's recommendation)

### 1.1.0

Added:		Optional integration with [SES Proxy](https://gitlab.com/gcdtech/ses-proxy)
Added:      Bounce / Complaint handling (via SES Proxy)
Added:      Per domain rate limiting (via SES Proxy) 

### 1.0.7

Fixed:		Fixed issue with handling notifications not returning a response properly

### 1.0.6

Fixed:		Fixed issue with bounces not parsing correctly

### 1.0.5

Added:		Tracking events to tracking handler now include the email

### 1.0.2

Fixed:		Fixed bug for SDK 3

### 1.0.1

Fixed:		Changed API request payload for v3 of SDK

### 1.0.0

Added:		First version
